# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.
def guessing_game
  chosen_num = rand(1..100)
  guess_counter = 0
  user_guess = 0

  until user_guess == chosen_num
    puts "Guess a number between 1 and 100:"
    user_guess = gets.chomp.to_i

    if user_guess < chosen_num
      puts "You guessed #{user_guess}. This number is too low."
    else
      puts "You guessed #{user_guess}. This number is too high."
    end
    guess_counter += 1
  end

    puts "You guessed #{user_guess} which was the same as #{chosen_num}."
    puts "It took you #{guess_counter} tries to guess correctly."
end



# Write file_shuffler program that: * prompts the user for a file name *
# reads that file * shuffles the lines * saves it to the file
# "{input_name}-shuffled.txt".
#
# Hint: You could create a random number using the Random class, or you
# could use the shuffle method in Array.

def file_shuffler
  file_name = gets.chomp
  content = File.readlines("file_name")
  new_file = File.open("file_name--shuffled.txt", "w") do |f|
    f.puts content.shuffle
  end  
end
